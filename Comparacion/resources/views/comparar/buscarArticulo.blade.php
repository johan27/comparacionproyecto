@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Buscar</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('Comparar.store') }}">

                      


                        {{ csrf_field() }}






                        <div class="form-group{{ $errors->has('provincia') ? ' has-error' : '' }}">
                            <label for="categoría" class="col-md-4 control-label">Categoría de Producto</label>

                            <div class="col-md-6">
                                

                            <select name="categoria" class="form-control" >
  <option value="Computadora">Computadora</option>
  <option value="lavadora">lavadora</option>
  <option value="Impresora">Impresora</option>
<option value="Pantalla">Pantalla</option>
  <option value="Maquina de cafe">Maquina de cafe</option>
    <option value="Celular">Celular</option>
      <option value="Radio">Radio</option>
</select>

                            </div>
                        </div>







                                <button type="submit" class="btn btn-primary">
                                    Buscar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
