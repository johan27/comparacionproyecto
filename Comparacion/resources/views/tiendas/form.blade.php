<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Nombre:</strong>
            {!! Form::text('nombre', null, array('placeholder' => 'Nombre','class' => 'form-control')) !!}
            @if ($errors->has('nombre'))
                <span class="help-block">
                    <strong>{{ $errors->first('nombre') }}</strong>
                </span>
            @endif
        </div>
    </div>
















    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>codigo:</strong>
            {!! Form::text('codigo', null, array('placeholder' => 'codigo','class' => 'form-control')) !!}
            @if ($errors->has('simbolo'))
                <span class="help-block">
                    <strong>{{ $errors->first('codigo') }}</strong>
                </span>
            @endif
        </div>
    </div>

    
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>ubicacion:</strong>
            {!! Form::text('ubicacion', null, array('placeholder' => 'ubicacion','class' => 'form-control')) !!}
            @if ($errors->has('descripcion'))
                <span class="help-block">
                    <strong>{{ $errors->first('ubicacion') }}</strong>
                </span>
            @endif
        </div>
    </div>



   
    {!! Form::hidden('user_id', Auth::id() )!!}
    
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</div>