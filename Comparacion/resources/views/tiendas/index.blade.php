
@extends('layouts.master')
@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Tiendas</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('tienda.create') }}">Crear una tienda</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Nombre</th>
            <th>codigo</th>
            <th>ubicacion</th>
            
            <th width="280px">Action</th>
        </tr>
            @foreach ($tiendas as $tienda)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $tienda->nombre}}</td>
        <td>{{ $tienda->codigo}}</td>
        <td>{{ $tienda->ubicacion}}</td>
       
       
        <td>
         <a class="btn btn-info" href="{{ route('tienda.show',$tienda->id) }}">Consultar</a>
            <a class="btn btn-primary" href="{{ route('tienda.edit',$tienda->id) }}">Editar</a>
            {!! Form::open(['method' => 'DELETE','route' => ['tienda.destroy', $tienda->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>

 {!! $tiendas->links() !!}
 
@endsection