@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Crear tienda</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('tienda.index') }}"> Volver</a>
            </div>
        </div>
    </div>
   
    {!! Form::open(array('route' => 'tienda.store','method'=>'POST')) !!}
         @include('tiendas.form')
    {!! Form::close() !!}
@endsection