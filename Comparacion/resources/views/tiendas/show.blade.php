@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Tienda</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('tienda.index') }}">Volver</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>tienda:</strong>
                @php
                   echo $tiendas['nombre'] . ' ('. $tiendas['codigo'] .')';
                @endphp
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre :</strong>
                {{ $tiendas->nombre}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>codigo:</strong>
                {{ $tiendas->codigo}}
            </div>
        </div>

           <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Ubicacion:</strong>
                {{ $tiendas->ubicacion}}
            </div>
        </div>
          
    </div>
@endsection