
@if ($item['submenu'] == [])
    <li>
        <a href="{{ url('listado',['id' => $item['id']]) }}">{{ $item['descripcion'] }}</a>
    </li>
@else
    <li class="dropdown">
        <a href="{{ url('listado',['id' => $item['id']]) }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ $item['descripcion'] }}<span class="caret"></span></a>
        
        <ul class="dropdown-menu sub-menu">
            @foreach ($item['submenu'] as $submenu)
                @if ($submenu['submenu'] == [])
                    <li><a href="{{ url('listado',['id' => $submenu['id']]) }}">{{ $submenu['descripcion'] }} </a></li>
                @else
                    @include('layouts.menu-item', [ 'item' => $submenu ])
                @endif
            @endforeach
        </ul>
    </li>
@endif