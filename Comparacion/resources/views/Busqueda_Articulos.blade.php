@extends('layouts.master')

@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Articulo</h2>
            </div>
            <div class="pull-right">
               
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Nombre_articulo</th>
            <th>categoria</th>
            <th>precio</th>
            <th>id_Tienda</th>
            
            
        </tr>
            @foreach ($Articulos as $Articulo)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $Articulo->nombre_articulo}}</td>
        <td>{{ $Articulo->categoría}}</td>
        <td>{{ $Articulo->precio}}</td>
        <td>{{ $Articulo->id_Tienda}}</td>
       
      
    </tr>
    @endforeach
    </table>

 {!! $Articulos->links() !!}
 
@endsection