@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Crear Articulo</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('Articulo.index') }}"> Volver</a>
            </div>
        </div>
    </div>
   
    {!! Form::model($tiendas,array('route' => 'Articulo.store','method'=>'POST')) !!}










         @include('Articulo.form')
    {!! Form::close() !!}
@endsection