@extends('layouts.master')

@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Articulos</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('Articulo.create') }}">Crear un Articulo</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Nombre_articulo</th>
            <th>categoria</th>
            <th>precio</th>
            
            <th>nombre tienda</th>
            <th>imagen</th>
            
            <th width="280px">Action</th>
        </tr>
               @foreach ($Articulos as $Articulo)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $Articulo->nombre_articulo}}</td>
        <td>{{ $Articulo->categoria}}</td>
        <td>{{ $Articulo->precio}}</td>
      

        <td>{{ $Articulo->id_Tienda}}</td>

<?php
//aquí coges de donde sea la ruta que quieres mostrar
$ruta=$Articulo->imagen;

?>



        <td><img src="<?php echo $ruta; ?>" width='238' height='100'>  
       </td>




       
        <td>
         <a class="btn btn-info" href="{{ route('Articulo.show',$Articulo->id) }}">Consultar</a>
            <a class="btn btn-primary" href="{{ route('Articulo.edit',$Articulo->id) }}">Editar</a>
            {!! Form::open(['method' => 'DELETE','route' => ['Articulo.destroy', $Articulo->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>

 {!! $Articulos->links() !!}
 
@endsection