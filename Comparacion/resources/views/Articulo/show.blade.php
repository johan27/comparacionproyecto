@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Articulo</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('Articulo.index') }}">Volver</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Articulo:</strong>
                @php
                 echo $Articulo['nombre_articulo'] . ' ('. $Articulo['precio'] .')';
                @endphp
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nombre_articulo :</strong>
                {{ $Articulo->nombre_articulo}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>categoria:</strong>
                {{ $Articulo->categoría}}
            </div>
        </div>

           <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>precio:</strong>
                {{ $Articulo->precio}}
            </div>
        </div>
          

          <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>id_tienda:</strong>
                {{ $Articulo->id_Tienda}}
            </div>
        </div>
    </div>
@endsection