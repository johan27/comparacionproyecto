
@extends('layouts.master')
@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Tiendas</h2>
            </div>
            <div class="pull-right">
                
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Nombre</th>
            <th>codigo</th>
            <th>ubicacion</th>
     </tr>
            @foreach ($tiendas as $tienda)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $tienda->nombre}}</td>
        <td>{{ $tienda->codigo}}</td>
        <td>{{ $tienda->ubicacion}}</td>
                      
    </tr>

    @endforeach
    </table>
    
 {!! $tiendas->links() !!}
 
@endsection