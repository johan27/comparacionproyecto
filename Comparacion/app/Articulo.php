<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    //


protected $fillable = [
        'nombre_articulo','categoría','precio','id_Tienda'
        
    ];
    
     public function user()
    {
        return $this->belongsTo('App\User');
    }


}
