<?php

namespace App\Http\Controllers;




use Illuminate\Http\Request;
use App\Articulos;
use App\tiendas;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class Articulo extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

     public function index()
    {
        $Articulos = Articulos::paginate(5);



        return view('Articulo.index',compact('Articulos'))

            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

        public function create()
    {





        $tiendas=tiendas::all();


        return view('Articulo.create',compact('tiendas'));

    }

         public function buscar($nombre_articulo)
    {
        
        $Articulos = Articulos::find($nombre_articulo);

      




     return view('Busqueda_Articulos',compact('Articulos'));



    }
    






    public function store(Request $request)
    {
$this->validate($request, [
            'nombre_articulo' => 'required',
            'categoria' => 'required',
            'precio' => 'required',
              'imagen' => 'required',


            'id_Tienda' => 'required',
         
        ]);


        Articulos::create($request->all());


        return redirect()->route('Articulo.index')
                        ->with('success','Articulo creado correctamente');
    }
    public function show($id)
    {

        $Articulo = Articulos::find($id);
        
        return view('Articulo.show',compact('Articulo'));
    }

 public function edit($id)
    {
        $Articulo = Articulos::find($id);





        $tiendas=tiendas::all();





        return view('Articulo.edit',compact('Articulo','tiendas'));    
    }
       public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre_articulo' => 'required',
            'categoria' => 'required',
            'precio' => 'required',
            'imagen' => 'required',
            'id_Tienda' => 'required',
         
        ]);

        Articulos::find($id)->update($request->all());
        return redirect()->route('Articulo.index')
                        ->with('success','Articulo actualizada correctamente');
    }

       public function destroy($id)
    {
        Articulos::find($id)->delete();
        return redirect()->route('Articulo.index')
                        ->with('success','Articulo eliminada correctamente');
    }
    

}
