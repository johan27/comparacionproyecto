<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tiendas;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;


class tienda extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');

      ///  $this->middleware('admin');

    }

     public function index()
    {
        $tiendas = tiendas::paginate(5);




        return view('tiendas.index',compact('tiendas'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }
      public function indexs()
    {
        $tiendas = tiendas::paginate(5);


        return view('tienda',compact('tiendas'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }


        public function create()
    {
        return view('tiendas.create');

    }
    public function store(Request $request)
    {

$this->validate($request, [
            'nombre' => 'required',
            'codigo' => 'required',
            'ubicacion' => 'required',
            
        ]);

        tiendas::create($request->all());


        return redirect()->route('tienda.index')
                        ->with('success','tienda creada correctamente');
    }
    public function show($id)
    {

        $tiendas = tiendas::find($id);
        
        return view('tiendas.show',compact('tiendas'));
    }

 public function edit($id)
    {
        $tiendas = tiendas::find($id);
        return view('tiendas.edit',compact('tiendas'));    
    }
       public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'codigo' => 'required',
            'ubicacion' => 'required',
            
         
        ]);

        tiendas::find($id)->update($request->all());
        return redirect()->route('tienda.index')
                        ->with('success','tienda actualizada correctamente');
    }

       public function destroy($id)
    {
        tiendas::find($id)->delete();
        return redirect()->route('tienda.index')
                        ->with('success','tienda eliminada correctamente');
    }
    






    
}
