<?php

namespace App\Http\Middleware;

use Closure;

use Alert;

class AdminMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


         if (auth()->check() && auth()->user()->Admin)
        return $next($request);


       ////  alert()->error('Solo el usuario administrador tiene acceso')->autoclose(3200);
               /// return response('Unauthorized.', 401);


        return Redirect('/');



        }



}


