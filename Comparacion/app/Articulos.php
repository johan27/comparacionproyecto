<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articulos extends Model
{


protected $fillable = [
         
        'nombre_articulo','categoria','precio','imagen','id_Tienda'
        
    ];
    
     public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    //
}
