<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArticuloAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
                Schema::create('Articulos', function (Blueprint $table) {
            $table->increments('id');
                        $table->string('nombre_articulo');
                          $table->string('categoria');
                          $table->string('precio');
                            $table->string('id_Tienda');
                            $table->string('imagen');
                            $table->foreign('id_Tienda')->references('id')->on('tiendas');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Articulos');
    }
}
